(defun make-player-scores (number-of-players)
  (make-array number-of-players))

(defparameter +marbles+ 72059)

(defparameter *game-circle* (make-array 1 :adjustable T))

(defun insert-element (n p arr)
  "Takes a n element and inserts in into the p position of an array by
moving all other elements to the right"
  (concatenate 'vector  (subseq arr 0 p)
               (make-array 1 :initial-contents `(,n))
               (subseq arr p)))

(defun add-marble (marble-no c-index game-circle)
  (cond
    ((= (length game-circle) 1) (values
				 (insert-element marble-no 1 game-circle)
				 1))
    ((= (+ 2 c-index) (length game-circle)) (values
					     (insert-element marble-no
							     (+ 2 c-index)
							     game-circle)
					     (+ 2 c-index)))
    (t (let ((offset (if (= marble-no 0)
		       0
		       (mod (+ c-index 2) (length game-circle)))))
       (values (insert-element marble-no
			       offset
			       game-circle)
	       offset)))))

(defun remove-element (p arr)
  "Takes the position of an element and returns an array without it"
  (concatenate 'vector (subseq arr 0 p)
	       (subseq arr (+ 1 p))))

(defun lucky-marble (marble-no c-index game-circle)
  (let* ((offset (mod (- c-index 7) (length game-circle)))
	 (points (+ marble-no (aref game-circle offset))))
    (values (remove-element offset game-circle)
	    points
	    offset)))

(defun player-of-marble (marble-no number-of-players)
  (mod (- marble-no 1) number-of-players))

(defun play-game (marbles number-of-players)
  (let ((player-scores (make-player-scores number-of-players)))
   (loop for i from 0 to (+ 1 marbles)
      with c-index = 0
      with game-circle = #()
      if (and (= 0 (mod i 23)) (not (= i 0)))
      do (multiple-value-bind (g-c p o)
	     (lucky-marble i c-index game-circle)
	   (setf game-circle g-c
		 (aref player-scores (player-of-marble i number-of-players))
		 (+ p (aref player-scores (player-of-marble i number-of-players)))
		 c-index o))
      else
      do (multiple-value-bind (g-c o)
	     (add-marble i c-index game-circle)
	   (setf game-circle g-c
		 c-index o)))
   player-scores))

(defun max-score (player-scores)
  (loop for i across player-scores
     maximizing i))

(max-score (play-game 72059 410)) ;=> 429487 (correct but painfully slow)

;; Another faster take
(ql:quickload '(:rutils :dlist))
