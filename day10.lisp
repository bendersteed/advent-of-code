;; Part 1 & 2
(ql:quickload '(:cl-ppcre))

(defstruct star x y vx vy)

(defun parse-input ()
  (with-open-file (in "input10.txt")
    (loop for line = (read-line in nil) while line
          for (x y vx vy) = (mapcar #'parse-integer (ppcre:all-matches-as-strings "(-?\\d+)" line))
          collect (make-star :x x :y y :vx vx :vy vy))))

(defun bbox (stars)
  (loop for s in stars
        minimize (star-x s) into left
        maximize (star-x s) into right
        minimize (star-y s) into top
        maximize (star-y s) into bottom
        finally (return (values left right top bottom))))

(defun update (stars)
  (loop for s in stars
        do (incf (star-x s) (star-vx s))
           (incf (star-y s) (star-vy s))
        finally (return stars)))

(defun draw (stars)
  (multiple-value-bind (left right top bottom) (bbox stars)
    (loop with width = (1+ (abs (- left right)))
          with height = (1+ (abs (- top bottom)))
          with field = (make-array (list width height) :initial-element #\.)
          for s in stars
          for x = (- (star-x s) left)
          for y = (- (star-y s) top)
          do (setf (aref field x y) #\#)
          finally (loop for row below height
                        do (loop for col below width
                                 do (format t "~c" (aref field col row)))
                           (format t "~%")))))

(defun main ()
  (loop with upper-bound = 20
        for stars = (parse-input) then (update stars)
        for secs from 0
        for (left right top bottom) = (multiple-value-list (bbox stars))
        for width = (abs (- left right))
        for height = (abs (- top bottom))
        for min-prev = min-dim
        minimize (min width height) into min-dim
        when (< min-dim min-prev upper-bound)
          do (format t "Possible result 10a:~%")
             (draw stars)
             (format t "Possible result 10b: ~d~%" secs)
        when (and (>= (min width height) upper-bound)
                  (< min-dim upper-bound))
     do (return)))
