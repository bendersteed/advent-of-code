;; Part 1
(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
       while line
       collect line)))

(defun split-by-char (string chr)
"Split the given string into a list of strings, on the given char"
  (loop for i = 0 then (1+ j)
          as j = (position chr string :start i)
          collect (subseq string i j)
          while j))

(defparameter +input+ (get-file "input3.txt"))

(defparameter +dimensions+ (loop for s in
      (loop for i in +input+
	 collect (cadddr (split-by-char i #\Space)))
    collect (split-by-char s #\x)))

(defparameter +coordinates+ (loop for s in
      (loop for i in +input+
	 collect (caddr (split-by-char i #\Space)))
    collect (split-by-char (string-trim ":" s) #\,)))

(defparameter *fabric* (make-array '(1000 1000)))

(defun nclaim-fabric (coordinates dimensions fabric)
  "Given the coordinates of the first square inch and the size of the
square mark the fabric, so that it turns to 1 if it was
non-claimed (O), 8 if it was once claimed (1), or do nothing if it was
already more than twice claimed (8)"
  (let ((x (parse-integer (car coordinates)))
	(y (parse-integer (cadr coordinates)))
	(width (parse-integer (car dimensions)))
	(height (parse-integer (cadr dimensions))))
    (loop for i from x to (+ x (1- width))
       do (loop for j from y to (+ y (1- height))
	     if (= (aref fabric j i) 0)
	     do (setf (aref fabric j i) 1)
	     else
	     do (setf (aref fabric j i) 8)))))

(loop for d in +dimensions+
   for c in +coordinates+
   do (nclaim-fabric c d *fabric*))

(count 8 (array-storage-vector *fabric*)) ;=> 110195,
					  ;array-storage-vector is an
					  ;sbcl extension

;; Part 2

(defun claim-check (coordinates dimensions fabric)
  "Given the coordinates and dimensions of a claim and the fabric
constructed to show it shows all the overlaps return true for a
non-overlaping claim."
  (let ((x (parse-integer (car coordinates)))
	(y (parse-integer (cadr coordinates)))
	(width (parse-integer (car dimensions)))
	(height (parse-integer (cadr dimensions))))
    (loop for i from x to (+ x (1- width))
       always (loop for j from y to (+ y (1- height))
		 always (= (aref fabric j i) 1)))))

(loop for i from 1 to (length +coordinates+)
   for c in +coordinates+
   for d in +dimensions+
   if (claim-check c d *fabric*)
     do (format t "The claim I support is: ~d" i)) ;=> The claim I
						   ;support is: 894
