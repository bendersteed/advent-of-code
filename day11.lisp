;; Part 1

(defparameter +serial-number+ 9810)

(defun power-level (x y)
  (let* ((rack-id (+ x 10))
	 (n (* rack-id (+ +serial-number+ (* y rack-id))))
	 (i (if (< n 99)
		0
		(let ((s (write-to-string n)))
		  (digit-char-p (aref s (- (length s) 3)))))))
    (- i 5)))

(defparameter *grid*
  (make-array '(300 300)))

(defun make-power-grid ()
  (loop for x from 0 to 299
     do (loop for y from 0 to 299
	   do (setf (aref *grid* y x) (power-level x y)))))

(defun sub-matrix-3x3 (arr i j)
  "Gives a 3x3 submatrix of a multidimensional array given the
  coordinates(x,y) of the top left element"
  (let ((new (make-array '(3 3))))
    (loop for x from 0 to 2
       for x1 from j to (+ 2 j)
       do (loop for y from 0 to 2
	       for y1 from i to (+ 2 i)
	     do (setf (aref new y x) (aref arr y1 x1))))
    new))

(defun sum-power-grid (arr)
  (let ((vec (array-storage-vector arr)))
    (loop for i across vec
       sum i)))

(defun sums-3x3 (arr)
  "Takes an array of power-levels and returns the power sum of
all 3x3 sub-matrices and their starting element "
  (loop for i from 0 to 297
     collect (loop for j from 0 to 297
		collect (list (sum-power-grid (sub-matrix-3x3 arr j i)) j i))))

(loop for j from 0 to 297
   collect (reduce (lambda (c d) (if (> (car c) (car d)) c d)) (loop for i from 0 to 297
	 collect (reduce (lambda (a b) (if (> (car a) (car b)) a b)) (nth i (sums-3x3 *grid*))))))


;; Part 2

(defun sub-matrix-nxn (arr n i j)
  "Gives a 3x3 submatrix of a multidimensional array given the
  coordinates(x,y) of the top left element"
  (let ((new (make-array `(,n ,n))))
    (loop for x from 0 to (1- n)
       for x1 from j to (+ (1- n) j)
       do (loop for y from 0 to (1- n)
	       for y1 from i to (+ (1- n) i)
	     do (setf (aref new y x) (aref arr y1 x1))))
    new))
