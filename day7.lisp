; Part 1
(ql:quickload 'cl-digraph)
(ql:quickload 'cl-digraph.dot)


(defun parse-input (file)
  (with-open-file (in file)
   (loop for line = (read-line in nil)
      while line
      collect (list (aref line 5) (aref line 36)))))

(defparameter *series-of-actions*
  (digraph:make-digraph))

(loop for i in (parse-input "input7.txt")
   do (progn (digraph:insert-vertex *series-of-actions* (car i))
	     (digraph:insert-vertex *series-of-actions* (cadr i))
	     (digraph:insert-edge *series-of-actions* (cadr i) (car i))))

(defun print-successors (vertex)
  (let ((deps (digraph:successors *series-of-actions* vertex)))
    (format t "~A depends on ~A~%" vertex deps)))

(digraph:map-vertices #'ser-successors *series-of-actions*)

(digraph.dot:draw *series-of-actions* :filename "digraph.png" :format :png)

(defun part1-solution (digraph)
    (concatenate 'string
     (loop while (not (digraph:emptyp digraph))
	collect (let* ((available (loop for v in (digraph:vertices digraph)
				     unless (digraph:successors digraph v)
				     collect v))
		       (sorted (sort available #'char<))
		       (step (car sorted)))
		  (digraph:remove-vertex digraph step)
		  step))))

(part1-solution *series-of-actions*)

