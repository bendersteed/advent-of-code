;; part 1

(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
       while line
       collect line)))

(defun count-chars-in-string (str)
  "Gets a string and returns a list of the numbers each
of its chars appears."
  (loop for i across str
     collect (count i str)))

(defun count-chars-in-strings (lst)
  "Gets a list of strings and returns a list of lists
of the numbers each char appears"
  (loop for s in lst
     collect (count-chars-in-string s)))

(defun count-doubles (lst)
  "Gets a list produced by count-chars-in-strings
and returns the count of words that contain doubles"
  (loop for i in lst
       count (member 2 i)))

(defun count-triples (lst)
  "Gets a list produced by count-chars-in-strings
and returns the count of words that contain triples"
  (loop for i in lst
     count (member 3 i)))


(let ((lst-frequencies (count-chars-in-strings (get-file "input2.txt"))))
  (* (count-doubles lst-frequencies) (count-triples lst-frequencies))) ;=> 5952

;; part 2

(defun common-chars (s1 s2)
  "Returns a list of common chars of two strings"
  (loop for c1 across s1
     for c2 across s2
     if (char-equal c1 c2)
       collect c1))

(let ((lst (get-file "input2.txt")))
 (loop for s1 in lst
    append (loop for s2 in lst
	      if (= (length (common-chars s1 s2)) 25)
	      collect (common-chars s1 s2))))

