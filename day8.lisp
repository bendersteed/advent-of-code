;; Part 1
(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
       while line
       collect line)))

(defun split-by-char (string chr)
"Split the given string into a list of strings, on the given char"
  (loop for i = 0 then (1+ j)
        as j = (position chr string :start i)
        collect (subseq string i j)
     while j))

(defparameter +license-numbers+
  (loop for i in (split-by-char (car (get-file "input8.txt")) #\Space)
     collect (parse-integer i)))

(defstruct node
  (children nil)
  (metadata nil))

(defun read-header (lst)
  "Header is the first two elements of a node"
  (list (car lst) (cadr lst)))

(defun parse-nodes (lst nodes)
  (let ((current lst)
	(children nil))
    (loop for x from 0 below nodes do
	 (let* ((h (read-header current))
		(n (car h))
		(m (cadr h))
		(rest (cddr current))
		(subchildren nil))
	   (when (> n 0)
	     (multiple-value-bind (c r) (parse-nodes rest n)
	       (setf subchildren c rest r)))
	   (push (make-node :children subchildren :metadata (subseq rest 0 m)) children)
	   (setf current (subseq rest m))))
    (values (reverse children) current)))

(defun sum-metadata (root)
  (+ (apply #'+ (node-metadata root))
     (loop for n in (node-children root) summing (sum-metadata n))))

(sum-metadata (car (parse-nodes +license-numbers+ 1))) ;=> 40036

;; Part 2
(defun sum-node (root)
  (cond
    ((null root) 0)
    ((null (node-children root)) (apply #'+ (node-metadata root)))
    (t (sum-nodes (mapcar #'1- (node-metadata root)) (node-children root)))))

(defun sum-nodes (indexes nodes)
  (loop for i in indexes summing (sum-node (nth i nodes))))

(sum-node (car (parse-nodes +license-numbers+ 1))) ;=> 21677
