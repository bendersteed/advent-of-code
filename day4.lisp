;; Part 1
(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
	  while line
	  collect line)))

(defparameter +input+ (get-file "input4.txt"))

(defparameter +sorted-input+
  (sort (loop for i in +input+
	      collect i)
	#'string<))

(defun parse-input (sorted-in)
  "Take a sorted input of Guard sleep times and return a list of Guard
  id sleeping time and waking time"
  (loop for entry in sorted-in
     for char = (char entry 25) ;if char # then new guard, if a sleep, if u wake
     for id = (if (char-equal char #\#)
		  (parse-integer entry :start 26 :junk-allowed t)
		  id)
     for a-time = w-time
     for w-time = (parse-integer entry :start 15 :end 17)
     when (char-equal char #\u) collect (list id a-time w-time)))

(defun asleep-hash (parse-in)
  "Takes a parsed input of notes and returns a hash map of Guards with
  arrays of length 60 that represent a minute, if 0 awake if 1
  asleep"
  (let ((asleep (make-hash-table)))
       (loop for note in parse-in
	  for (arr exists) =
	    (multiple-value-list (gethash (first note) asleep (make-array 60)))
					; return the array if
					; the key exists in asleep
					; else the arr is an empty
					; one, exists holds t or nil
					; respectively
	  do (loop for i from (second note) below (third note)
		do (incf (aref arr i)))
	    (unless exists
	      (setf (gethash (first note) asleep) arr)))
       asleep))

(defun solution-to-part-1 (hash)
  (destructuring-bind (id minutes sum)
      (reduce (lambda (a b) (if (> (third a) (third b)) a b))
	      (loop for key being the hash-keys of hash
		 using (hash-value value)
		 collect (list key value (reduce #'+ value))))
    (* id (position (reduce #'max minutes) minutes))))

;; Part 2
(defun solution-to-part-2 (hash)
  (destructuring-bind (id minutes max)
      (reduce (lambda (a b) (if (> (third a) (third b)) a b))
	      (loop for key being the hash-keys of hash
		 using (hash-value value)
		 collect (list key value (reduce #'max value))))
    (* id (position max minutes))))

(defparameter *asleep* (asleep-hash (parse-input +sorted-input+)))
(solution-to-part-1 *asleep*) ;=> 87681
(solution-to-part-2 *asleep*) ;=> 136461
