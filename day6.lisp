;; Part 1
(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
	  while line
	  collect line)))

(defun split-by-char (string chr)
"Split the given string into a list of strings, on the given char"
  (loop for i = 0 then (1+ j)
        as j = (position chr string :start i)
        collect (subseq string i j)
        while j))

(defparameter +input+ (loop for i in (get-file "input6.txt")
			    collect (split-by-char i #\Space)))
