;; Part 1
(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
       while line
       collect line)))

(defvar confs
  (mapcar #'parse-integer (get-file "input1.txt")))

(reduce #'+ confs) ; => 556

;; Part 2

(defun if-seen-twice (lst freq seen)
  "Adds the elements recursively till it finds the same sum twice,
recurses the lst from start if not found"
  (cond
    ((null lst) (if-seen-twice confs freq seen))
    ((member freq seen) freq)
    (t (if-seen-twice (cdr lst)
		      (+ freq (car lst))
		      (append seen (list freq))))))

(if-seen-twice confs 0 '()) ; =>448

