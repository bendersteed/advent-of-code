;; Part 1
(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
	  while line
	  collect line)))

(defun split (string)
"Split the given string into a list of characters"
  (loop for i across string
	collect i))

(defparameter +input+
  (split (car (get-file "input5.txt"))))

(defun react-p (c1 c2)
  "Takes two chars and checks if they react if they represent a
polymer element"
  (and (char-equal c1 c2) (not (eql c1 c2))))

(defun reaction (polymer start)
  "Takes a list of chars that defines a polymer sequence and executes
a pass reactions"
  (let ((x (car polymer))
	(y (cadr polymer)))
      (cond
	((null (cdr polymer)) (if (null x)
				  start
				  (reverse (append `(,x) start))))
	((react-p x y) (reaction (cddr polymer) start))
	(t (reaction (cdr polymer) (append `(,x) start))))))

(defun reactions (polymer)
  "Computes all passes of reactions"
  (let ((pass (reaction polymer '())))
    (if (= (length pass) (length polymer))
	(length pass)
	(reactions pass))))

(reactions +input+) ;=> 9238

;;Part 2
(defun remove-unit (polymer char)
  "Takes a polymer and a unit and returns a polymer that doen't have
  the unit in."
  (loop for u in polymer
	unless (char-equal u char)
	  collect u))

(defparameter +units+
  '(#\a #\b #\c #\d #\e #\f #\g #\h #\i #\j #\k
    #\l #\m #\n #\o #\p #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z))

(defun min-polymer (polymer)
  "Compute the shortest polymer post reactions given that we remove a
  single unit"
  (loop for u in +units+
	minimize (reactions (remove-unit polymer u))))

(min-polymer +input+) ;=> 4052
