;; Part 1
(defun split-by-char (string chr)
"Split the given string into a list of strings, on the given char"
  (loop for i = 0 then (1+ j)
          as j = (position chr string :start i)
          collect (subseq string i j)
          while j))

(with-open-file (stream "input12.txt")
  (defparameter +initial-state+
    (concatenate 'list
		 '(0 0 0 0 0)
		 (let ((line (read-line stream)))
		   (loop for c across line
		      if (char-equal c #\#)
		      collect 1
		      else
		      collect 0))
		 '(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0))))

(with-open-file (stream "rules12.txt")
  (defparameter +rules+
    (loop for line = (read-line stream nil 'eof)
       until (eq line 'eof)
       collect (let* ((test (loop for i from 0 to 4
			       if (char-equal (aref line i) #\#)
			       collect 1
			       else
			       collect 0)))
		 (if (char-equal (aref line 9) #\#)
		     (append '(1) test)
		     (append '(0) test))))))

(defparameter test-state
  '(0 0 0 1 0 0 1 0 1 0 0 1 1 0 0 0 0 0 0 1 1 1 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0))

(with-open-file (stream "test12.txt")
  (defparameter +test-rules+
    (loop for line = (read-line stream nil 'eof)
       until (eq line 'eof)
       collect (let* ((test (loop for i from 0 to 4
			       if (char-equal (aref line i) #\#)
			       collect 1
			       else
			       collect 0)))
		 (if (char-equal (aref line 9) #\#)
		     (append '(1) test)
		     (append '(0) test))))))

(defun pot-env (i state)
  "Takes the pot at ith positions at state and returns its
  environment"
  (let ((len (length state)))
   (cond
     ((= i 0) (append '(0 0) (subseq state 0 (+ i 3))))
     ((= i 1) (append '(0) (subseq state 0 (+ i 3))))
     ((= i (1- len))
      (append (subseq state (- i 2) len) '(0 0)))
     ((= i (- len 2))
      (append (subseq state (- i 2) len) '(0)))
     (t (subseq state (- i 2) (+ i 3))))))

(defun pot-next-gen (i state rules)
  "Takes the index of a pot in a state and set of rules and returns 0
  or 1 depending if it will have a plant in the next generation"
  (let* ((env (pot-env i state))
	 (res (loop for rule in rules
	     if (equal (cdr rule) env)
		 return (car rule))))
    (if res
	res
	0)))

(defun play-sim (state rules)
  (loop for pot from 0 to (1- (length state))
     collect (pot-next-gen pot state rules)))

(defparameter +after-20-gen+ (let ((state +initial-state+))
			       (loop for i from 1 to 20
				  do (setf state (play-sim state +rules+)))
			       state))

(loop for i from 0 to (1- (length +after-20-gen+))
   if (= 1 (nth i +after-20-gen+))
   sum (- i 5))

